// Funciones a testear, originalmente estan en Operacion.js
function suma(num1,num2) {
  return num1+num2;
}

function resta(num1,num2){
  return num1 - num2;
}

function multiplicacion(num1,num2){
  return num1*num2;
}

function division(num1,num2){
  return num1/num2;
}

//Tests


describe('Test De la suma', () => {
  it('Se suman los numeros correctamente', () => {
      // Preparado
      var a = 1;
      var b = 1;
      var res = 2;

      // Ejecutar
      const result = suma(a,b);

      // Confirmacion
      expect(result).toBe(res);
  })
});

describe('Test De la resta', () => {
  it('Se restan los numeros correctamente', () => {
      // Preparado
      var a = 1;
      var b = 1;
      var res = 0;

      // Ejecutar
      const result = resta(a,b);

      // Confirmacion
      expect(result).toBe(res);
  })
});

describe('Test De la multiplicacion', () => {
  it('Se multiplican los numeros correctamente', () => {
      // Preparado
      var a = 1;
      var b = 1;
      var res = 1;

      // Ejecutar
      const result = multiplicacion(a,b);

      // Confirmacion
      expect(result).toBe(res);
  })
});

describe('Test De la Division', () => {
  it('Se dividen los numeros correctamente', () => {
      // Preparado
      var a = 4;
      var b = 2;
      var res = 2;

      // Ejecutar
      const result = division(a,b);

      // Confirmacion
      expect(result).toBe(res);
  })
});