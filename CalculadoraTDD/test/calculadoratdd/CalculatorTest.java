/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoratdd;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author AnX
 */
public class CalculatorTest {
    
    public CalculatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of suma method, of class Calculator.
     */
    @Test
    public void testSuma() {
        System.out.println("suma");
        int a = 1;
        int b = 1;
        Calculator instance = new Calculator();
        int expResult = 2;
        int result = instance.suma(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
    @Test
    public void testResta(){
        System.out.println("resta");
        int a = 1;
        int b = 1;
        Calculator instance = new Calculator();
        int expResult=0;
        int result = instance.resta(a,b);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testMultiplicacion(){
        System.out.println("multiplicacion");
        int a = 2;
        int b = 3;
        Calculator instance = new Calculator();
        int expResult=6;
        int result = instance.multiplicacion(a,b);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testDividir(){
        System.out.println("dividir");
        int a = 6;
        int b = 2;
        Calculator instance = new Calculator();
        int expResult=3;
        int result = instance.dividir(a,b);
        assertEquals(expResult, result);
    }
}
